from flask import Flask, request, render_template, url_for 
import pandas as pd
import tensorflow as tf
import random as rand
import keras

app = Flask(__name__)

# load dataset for random choice


@app.route('/', methods=['POST', 'GET'])


def dropdown():
    print(request.method)
    model = tf.keras.models.load_model(r"NNmodel_mlp")
    
    df = pd.read_csv("test_ff.csv")
    df = df.round(decimals=5)
    print('Type of df[0,3]:', type(df.iloc[0, 3]))         #  numpy format!! То есть потом при выборке преображение идет в экспонент.формат
    #df.set_index('id', inplace=True)
    y_pred = 0
    if request.method=='POST': 
        select = request.form['operator'] #post('operator')
       
        print('Length', len(select))   
        print('Type:', type(select))
        #print(select)
        text_file = open("sample.txt", "w")
        string_to_file = text_file.write(select)
        text_file.close()
        print(select[0]) 
        print('Type of select[0]', type(select[0]))
        print('Length of select[]:', len(select[0])) 
        print(type(select))
        print(select)
        select_splitted = select.strip('[').strip(']').split()   
     
        print('Type of select_splitted', type(select_splitted))
        print('Length of select_splitted', len(select_splitted))
        select_splitted_to_pdseries = pd.Series(select_splitted)
       
        #select_splitted_to_pdseries.to_csv('1')
        select_splitted_to_pdseries_list=[]
        for i in select_splitted_to_pdseries:
            print(i)
            print(type(i))               # КОММЕНТАРИЙ!! Видно в терминале при запуске приложения, что тип строка и из-за того, что строка, похоже, в экспонентоциальном формате !!
            
            select_splitted_to_pdseries_list.append(float(i))
        print('PdSeries[0] is', select_splitted_to_pdseries[0])
        print('Type of PdSeries[0] is', type(select_splitted_to_pdseries[0]))
        
        print(select_splitted_to_pdseries)
        
        #y_pred = model.predict([[select_splitted_to_pdseries]])
        select_splitted_to_pdseries = pd.Series(select_splitted_to_pdseries_list)
        
        y1 = select_splitted_to_pdseries.values.reshape(1,-1)
        
        y_pred = model.predict(y1)
        print(y_pred)
    return render_template('main.html', df = df, result=y_pred)

def main():
    if flask.request.method == 'GET':
        return render_template('main.html')

if __name__ == '__main__':
    app.run(debug=True)